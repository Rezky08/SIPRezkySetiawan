@prepend('head')
    <link rel="stylesheet" href="{{ asset('css/bulma.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bulma-tooltip.min.css') }}">
    <script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
@endprepend
